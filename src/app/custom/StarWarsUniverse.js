import Species from './Species';
import EventEmitter from 'eventemitter3';

export default class StarWarsUniverse extends EventEmitter {
  constructor(_maxSpecies) {
    super();
    this.species = [];
    this._maxSpecies = _maxSpecies;
  }
  static get events() {
    return {
      MAX_SPECIES_REACHED: 'max_species_reached',
      SPECIES_CREATED: 'species_created',
    };
  }

  get speciesCount() {
    return this.species.length;
  }
  _onSpeciesCreated(specie) {
    if (this._maxSpecies === this.speciesCount) {

      this.emit(StarWarsUniverse.events.MAX_SPECIES_REACHED);
    } else {
      this.species.push(specie);
      this.emit(StarWarsUniverse.events.SPECIES_CREATED, { speciesCount: this.speciesCount });
      this.createSpecies();
    }
  }

  createSpecies() {
    const specie = new Species();
    const currentSpecie = this.speciesCount + 1;

    specie.init(`https://swapi.booost.bg/api/species/${currentSpecie}/`);
    specie.on(StarWarsUniverse.events.SPECIES_CREATED, () => {
      this._onSpeciesCreated(specie);
    });
  }
}
